const pieChartData = {
    type: "pie",
    plugins: [ChartDataLabels],
    data: {
        labels: ["Firewallsdafegerg", "IPS1", "DRM1", "TESTdfagardfxvxv xfbxfbfgbbbggb", "DBMSzdfdgsf"],
        datasets: [
            {
                label: "label1",
                data: [30, 20, 10, 5, 3],
                backgroundColor: ["#2ec6c8", "#9f78e6", "#8bc9f6", "#fad3b3", "#f5f3ca"],
                datalabels: {
                    display: false
                }
            }
        ]
    },
    options: {
        title: {
            display: false
        },
        legend: false,
        legendCallback: function(chart) {
            let ul = document.createElement("ul");
            const backgroundColor = chart.data.datasets[0].backgroundColor;
            const data = chart.data.datasets[0].data;
            chart.data.labels.forEach(function(label, index) {
                ul.innerHTML += `
                <li>
                    <div class="d-flex flex-row align-items-center">
                        <div class="legend-color-container"><span style="background-color: ${backgroundColor[index]}"></span></div>
                        <span>${label}</span> 
                        <span class="ml-auto">${data[index]}%</span>
                    </div>
               </li>
            `;
            });
            return ul.outerHTML;
        }
    }
};

const pieCtx = document.getElementById("myPieChart").getContext("2d");
const myPieChart = new Chart(pieCtx, pieChartData);
const legend = document.getElementById("legend");
legend.innerHTML = myPieChart.generateLegend();
