const chartData = {
    type: "horizontalBar",
    plugins: [ChartDataLabels],
    data: {
        labels: ["분석로그", "백업로그", "당일수집로그"],
        datasets: [
            {
                label: "label1",
                data: [59, 10, 5],
                backgroundColor: "#4d62dd",
                datalabels: {
                    color: "#fff",
                    anchor: "end",
                    offset: 1,
                    font: {
                        size: 9,
                        lineHeight: 1,
                        family: "Nanum Barun Gothic"
                    },
                    align: "start",
                    formatter: function(value) {
                        return value + "%";
                    }
                }
            },
            {
                label: "label2",
                data: [41, 90, 95],
                backgroundColor: "#d8dae9",
                offset: 1,
                datalabels: {
                    color: "#b3b6ca",
                    anchor: "end",
                    font: {
                        size: 9,
                        lineHeight: 1,
                        family: "Nanum Barun Gothic"
                    },
                    align: "start",
                    formatter: function(value) {
                        return value + "%";
                    }
                }
            }
        ]
    },
    options: {
        title: {
            display: true,
            position: "bottom",
            text: "(%)",
            fontFamily: "Nanum Barun Gothic"
        },
        legend: {
            display: false
        },
        scales: {
            xAxes: [
                {
                    display: true,
                    position: "bottom",
                    stacked: true,
                    label: "(%)",
                    ticks: {
                        beginAtZero: true,
                        max: 100,
                        fontSize: 10,
                        stepSize: 20
                    },
                    gridLines: {
                        color: "#000",
                        display: false,
                        lineWidth: 2
                    }
                }
            ],
            yAxes: [
                {
                    stacked: true,
                    ticks: {
                        fontSize: 14
                    },
                    gridLines: {
                        color: "#000",
                        display: false,
                        lineWidth: 2
                    },
                    maxBarThickness: 20
                }
            ]
        }
    }
};

const ctx = document.getElementById("myChart").getContext("2d");
const myChart = new Chart(ctx, chartData);
